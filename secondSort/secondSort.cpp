#include <iostream>

using namespace std;
const int maxLength = 30;
int enterArray(int(&arr)[maxLength], int);
void displayArray(int[], int);
void swap(int&, int&);
void insertionSort(int(&arr)[maxLength], int);
void mergeSort(int(&arr)[maxLength], int, int);
void merge(int(&arr)[maxLength], int, int);

int main()
{
    int numbers[maxLength]{ 0 };
    int newLength = enterArray(numbers, maxLength);
    displayArray(numbers, newLength);
    cout << endl;
    mergeSort(numbers, 0, newLength - 1);
    displayArray(numbers, newLength);
}

int enterArray(int(&numbers)[maxLength], int length)
{
    int newLength;
    cout << "Enter number less than 30: " << endl;
    cin >> newLength;

    for (size_t i = 0; i < newLength; i++)
    {
        cout << "Enter " << i + 1 << "'s element: ";
        cin >> numbers[i];
    }
    return newLength;
}

void displayArray(int numbers[], int length)
{
    for (size_t i = 0; i < length; i++)
    {
        cout << numbers[i] << " ";
    }
}

void swap(int& first, int& second)
{
    int tmp = first;
    first = second;
    second = tmp;
}

void insertionSort(int (&numbers)[maxLength], int length)
{
    for (size_t i = 1; i < length; i++)
    {
        for (int j = i - 1; j >= 0; j--)
        {
            if (numbers[j+1] < numbers[j])
            {
                swap(numbers[j+1], numbers[j]);
            }
            else
            {
                break;
            }
        }
    }
}

void mergeSort(int(&numbers)[maxLength], int first, int last)
{
    if (first < last) // length is not 1
    {
        if (last - first == 1) // length == 2
        {
            if (numbers[first] > numbers[last])
            {
                swap(numbers[first], numbers[last]);
            }
        }
        else
        {
            mergeSort(numbers, first, first + (last - first) / 2); // from first to middle (not including)
            mergeSort(numbers, first + (last - first) / 2 + 1, last); // from middle (including) to last
            merge(numbers, first, last);
        }
    }
}

void merge(int(&numbers)[maxLength], int first, int last)
{
    int middle = first + (last - first) / 2,
        resultArray[maxLength]{ 0 },
        i = first,
        j = middle + 1,
        k = 0;

    while (i <= middle && j <= last)
    {
        if (numbers[i] < numbers[j])
        {
            resultArray[k] = numbers[i];
            i++;
        }
        else
        {
            resultArray[k] = numbers[j];
            j++;
        }
       k++;
    }

    while (i <= middle)
    {
        resultArray[k] = numbers[i];
        i++;
        k++;
    }

    while (j <= last)
    {
        resultArray[k] = numbers[j];
        j++;
        k++;
    }
    
    for (size_t l = 0; l < k; l++) 
    {
        numbers[first + l] = resultArray[l];
    }
}
