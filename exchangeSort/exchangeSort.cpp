#include <iostream>

using namespace std;

void exchangeSort(int (&arr)[30], int);
void selectionSort(int (&arr)[30], int);
int enterArray(int (&arr)[30], int);
void displayArray(int[], int);
void swap(int&, int&);

int main()
{
    const int templateLength = 30;
    int numbers[30]{ 0 };
    const int newLength = enterArray(numbers, templateLength);
    displayArray(numbers, newLength);
    //exchangeSort(numbers, newLength);
    cout << endl;
    selectionSort(numbers, newLength);
    displayArray(numbers, newLength);
}

int enterArray(int (&numbers)[30], int length)
{
    int newLength;
    cout << "Enter number less than 30: " << endl;
    cin >> newLength;

    for (size_t i = 0; i < newLength; i++)
    {
        cout << "Enter " << i+1 << "'s element: ";
        cin >> numbers[i];
    }
    return newLength;
}

void displayArray(int numbers[], int length)
{
    for (size_t i = 0; i < length; i++)
    {
        cout << numbers[i] << " ";
    }
}

void exchangeSort(int (&numbers)[30], int length)
{
    bool swapped = false;
    int alreadySorted = 0;
    do
    {
        swapped = false;
        for (size_t i = 1; i < length - alreadySorted; i++)
        { 
            if (numbers[i - 1] > numbers[i])
            {
                swap(numbers[i - 1], numbers[i]);
                swapped = true;
            }
        }
        alreadySorted++;
    } while (swapped);
}

void selectionSort(int (&numbers)[30], int length)
{
    int lastNotSorted = 0;
    int current_minimum = 0;

    for (size_t i = 0; i < length-1; i++)
    {
        current_minimum = lastNotSorted;
        for (size_t j = lastNotSorted; j < length; j++)
        {
            if (numbers[current_minimum] > numbers[j])
            {
                current_minimum = j;
            }
        }
        swap(numbers[lastNotSorted], numbers[current_minimum]);
        lastNotSorted++;
    }
}

void swap(int &first, int &second)
{
    int tmp = first;
    first = second;
    second = tmp;
}